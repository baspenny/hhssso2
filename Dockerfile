FROM python:3.7
ENV PYTHONUNBUFFERED 1
RUN apt-get update
RUN apt-get install libxml2-dev libxmlsec1-dev libxmlsec1-openssl -y
RUN apt-get install pkg-config -yy
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/